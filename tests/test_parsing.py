import pytest
from pyno import HTML as H


def test_iterator_unwrapping():
    page = H.ul(H.li(n) for n in range(1, 10))
    assert str(page) == '<ul><li>1</li><li>2</li><li>3</li><li>4</li><li>5</li><li>6</li><li>7</li><li>8</li><li>9</li></ul>', 'Default behavior is broken'
    assert str(page) == '<ul><li>1</li><li>2</li><li>3</li><li>4</li><li>5</li><li>6</li><li>7</li><li>8</li><li>9</li></ul>', 'Iterators might not be unrwapped correctly'


def test_html_beutify():
    from pyno.browser_preview import html_prettify
    page = H.ul(H.li(n) for n in range(1, 10))
    assert html_prettify(str(page)) == '<ul>\n <li>\n  1\n </li>\n <li>\n  2\n </li>\n <li>\n  3\n </li>\n <li>\n  4\n </li>\n <li>\n  5\n </li>\n <li>\n  6\n </li>\n <li>\n  7\n </li>\n <li>\n  8\n </li>\n <li>\n  9\n </li>\n</ul>', 'HTML beautyfication failed'


def test_default_arguments():
    H.defaults['div'] = {'color': 'Green'}
    assert str(H.div('test')) == '<div color="Green">test</div>', 'default arguments is broken'
    H.defaults['div'] = {}


def test_classymaker():
    assert str(H.div.card['mt-2']('something', Class='topdollar')) == '<div class="card mt-2 topdollar">something</div>', "There is an issue in the class parser"


def test_void_elements():
    str(H.div(H.link(), H.meta())) == '<div><link /><meta /></div>', "Void elements parsing incorrectly"


def test_custom_tags():
    #assert str(H.Import('file.js')) == '<script type="text/javascript" src="file.js"></script>', 'Problem in Import tag'
    #assert str(H.Include('file.js')) == '<script type="text/javascript" src="file.js"></script>', 'Problem in Include tag'
    #assert str(H.Include('file.css')) == '<link rel="stylesheet" type="text/css" href="file.css" />', 'Problem in Include tag'
    #assert str(H.Import('file.css')) == '<link rel="stylesheet" type="text/css" href="file.css" />', 'Problem in Import tag'
    assert str(H.Require('file.js')) == '<script type="text/javascript" src="file.js"></script>', 'Problem in Import tag'
    assert str(H.Require('file.js')) == '<script type="text/javascript" src="file.js"></script>', 'Problem in Include tag'
    assert str(H.Require('file.css')) == '<link rel="stylesheet" type="text/css" href="file.css" />', 'Problem in Include tag'
    assert str(H.Require('file.css')) == '<link rel="stylesheet" type="text/css" href="file.css" />', 'Problem in Import tag'
    assert str(H.CDATA('test')) == '<![CDATA[test]]>', "Problem in CDATA tag"

    with pytest.raises(ValueError):
        str(H.Require('file.png'))

    with pytest.raises(ValueError):
        str(H.Require('file.png'))


def test_custom_construct_class():
    class TestObj(H):
        def construct(self, name, *args, **kwargs):
            return H.div(name)

    assert str(H.TestObj('John')) == '<div>John</div>', 'Issue in defining costum elements'


def test_autocomplete_module():
    import pyno.html_tags_autocomplete
    tags = 'body title div'

    assert pyno.html_tags_autocomplete.definition_generator(tags) == \
'''class HTMLTagList:
    """Dummy class used to provide autocomplete for TreeSeed"""
    body = None
    title = None
    div = None
''', 'Issue in autocomplete module'


def test_signature():
    from pyno.tree_model import get_default_args

    def test(a, g, b=23, c='ok'):
        return None

    assert test(1, 2) == None, 'Test function failed'
    assert get_default_args(test) == {'b': 23, 'c':'ok'}, 'Error in signature parsing'


def test_style():
    from pyno.stylesheets import Style

    css = Style()
    css['div'] = dict(border='1px solid black', font_size=14)

    assert str(css) == 'div {\n  border: 1px solid black;\n  font-size: 14;\n}\n\n'



def test_style_css_variables():
    # todo slightly heavy test, consider splitting
    from pyno.stylesheets import Style

    css = Style()

    css.variable.location = 'center'

    css['div ul.btn'] = {
        'border_width': '1px',
        'alignment': css.variable.location,
        'background_color': css.variable.primarycolor("#8A64FF")}

    assert str(css.variable.location) == 'center', 'css var get attr has problem'
    assert str(css.variable.__getattribute__('location')) == 'center', 'css var get attr has problem'
    assert str(css.variable.__getattr__('location')) == 'center', 'css var get attr has problem'

    assert str(css) == 'div ul.btn {\n  border-width: 1px;\n  alignment: center;\n  background-color: #8A64FF;\n}\n\n', 'Initial css var failed'

    css.variable.location = 'right'
    css.variable.primarycolor('#440000')

    assert str(css) == 'div ul.btn {\n  border-width: 1px;\n  alignment: right;\n  background-color: #440000;\n}\n\n', 'css var reassignment failed'
    assert css.list_variables() == ['location', 'primarycolor'], 'Problem in variable listing'
    assert str(css.variable.location) == 'right', 'css var get attr has problem'

    css['div ul.btn'] += {'font-weight': 600}

    assert str(css) == 'div ul.btn {\n  border-width: 1px;\n  alignment: right;\n  background-color: #440000;\n  font-weight: 600;\n}\n\n', 'Problem in joinable dict extension'

def test_constructor_decorator():

    @H.construct
    def test(*args, **kwargs):
        return H.div('hello')

    assert str(H.test()) == '<div>hello</div>', 'Issue when using decorator constructor'


def test_treenode_attr_access():
    node = H.div('test', data_name='some name')
    node.data_name == 'some name', 'Data access on treenodes is not working'
    node.data_name = 'some other name'
    str(node) == '<div data-name="some other name">test</div>'


def namespace_test():   # pragma: no cover
    from pyno import Namespace

    gg = Namespace()
    gg.namespace = 'gg'

    print(str(gg.div('test')))
    # Should test lines 142-147 and 171

    # Also need test for construct decorator from namespace


def test_none_arguments():
    assert str(H.div('content', data=None)) == '<div data>content</div>', 'Error in handling of None args'
    assert str(H.div('content', data=None, time=None)) == '<div data time>content</div>', 'Error in handling of None args'