# Framework support overview

Frameworks supporeted and latest version included:

* [v] Bootstrap v4.4.1
* [v] Foundation v.6.4.3
* [v] Bulma v0.8.0
* [v] Materialize v1.0.0
* [v] Semantic UI v2.4.2
* [v] MaterialDesignLite v1.3.0
* [v] UIKit v3.2.7
* [v] Pure v1.0.1

[Relative comparisons of frameworks](https://www.altexsoft.com/blog/engineering/most-popular-responsive-css-frameworks-bootstrap-foundation-materialize-pure-and-more/)

## Note

Shell script for generating integrity hash for fies during setup.
```{shell }
curl "" | openssl dgst -sha384 -binary | openssl base64 -A | awk '{print "integrity=\"sha384-" $0 "\""}'
```
