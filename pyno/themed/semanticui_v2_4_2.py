"""
Semantic UI v2.4.2
"""

from pyno import HTML


HTML.defaults['head'] = {'_before': [
    HTML.Require("https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css", 
        integrity='sha384-JKIDqM48bt14NZpzl9v0AP36VK2C/X6RuSPfimxpoWdSANUXblZUX1cgdQw8cZUK',
        crossorigin="anonymous")]}

HTML.defaults['body'] = {'_after': [
    HTML.Require("https://code.jquery.com/jquery-3.1.1.min.js",
        integrity='sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7',
        crossorigin="anonymous"),
    HTML.Require("https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.js",
        integrity='sha384-HtPdw6a7QeUoz9fgdEIk3iL4WifPUg7BuiWndgaxmBDiCHMYfOO2nEJ9J3YPg6+S',
        crossorigin="anonymous")]}
