"""
Bulma v0.8.0
"""

from pyno import HTML


HTML.defaults['head'] = {'_before': [
    HTML.Require("https://cdnjs.cloudflare.com/ajax/libs/bulma/0.8.0/css/bulma.min.css",
    integrity="sha384-5kGFOAqgEKnuF6c2jk2JANKHZgYVt38Wn2dVJENtq1EBCP54/1uzXU5mpxqL8WjN",
    crossorigin="anonomous")]}
