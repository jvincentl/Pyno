"""
Materializecss v1.0.0
"""

from pyno import HTML


HTML.defaults['head'] = {'_before': [
    HTML.Require("https:///cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css", 
        integrity="sha384-5KZdSYqynSzIjQGS2M1O3HW6HVDBjfNx0v5Y2eQuE3vvQ9NTiiPK9/GWc0yYCrgw",
        crossorigin="anonymous")]}

HTML.defaults['body'] = {'_after': [
    HTML.Require("https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js",
        integrity="sha384-ZOED+d9HxogEQwD+jFvux96iGQR9TxfJO+mPF2ZS0TuKH6eWrmvPsDpO6I0OWdiX",
        crossorigin="anonymous")]}