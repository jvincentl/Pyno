"""
Foundation v6.4.3
"""

from pyno import HTML


HTML.defaults['head'] = {'_before': [
    HTML.Require("https://cdn.jsdelivr.net/npm/foundation-sites@6.4.3/dist/css/foundation.min.css",
        integrity="sha384-wAweiGTn38CY2DSwAaEffed6iMeflc0FMiuptanbN4J+ib+342gKGpvYRWubPd/+",
        crossorigin="anonymous")]}

HTML.defaults['body'] = {'_after': [
    HTML.Require("https://cdn.jsdelivr.net/npm/foundation-sites@6.4.3/dist/js/foundation.min.js",
        integrity="sha384-KzKofw4qqetd3kvuQ5AdapWPqV1ZI+CnfyfEwZQgPk8poOLWaabfgJOfmW7uI+AV",
        crossorigin="anonymous")]}
