"""
UI Kit v3.2.7
"""

from pyno import HTML


HTML.defaults['head'] = {'_before': [
    HTML.Require("https://cdn.jsdelivr.net/npm/uikit@3.2.7/dist/css/uikit.min.css", 
        integrity="sha384-VivMcFpgetOPVrT63+0Yd1XwO1+p5yAX1Oe12a1017P/Yy6GZet/kZsKFCf6YPVQ",
        crossorigin="anonomous")]}

HTML.defaults['body'] = {'_after': [
    HTML.Require("https://cdn.jsdelivr.net/npm/uikit@3.2.7/dist/js/uikit.min.js",
        integrity="sha384-ybowP14DiCBQtE8Exauz2TO2to3lokbNxt97FoPuivgodVcauneYTB8g90YMEISJ",
        crossorigin="anonomous"),
    HTML.Require("https://cdn.jsdelivr.net/npm/uikit@3.2.7/dist/js/uikit-icons.min.js",
        integrity="sha384-b2jhd8xnmLVLDc44TdLlN3n6c8P5qfCgOYwiOyyXvCcV2DI1kPpdJDHD0karMCCD",
        crossorigin="anonomous")]}