
"""
Pure v1.0.1
"""

from pyno import HTML


HTML.defaults['head'] = {'_before': [
    HTML.Require("https://unpkg.com/purecss@1.0.1/build/pure-min.css",
        integrity="sha384-oAOxQR6DkCoMliIh8yFnu25d7Eq/PHS21PClpwjOTeU2jRSq11vu66rf90/cZr47",
        crossorigin="anonymous")]}
