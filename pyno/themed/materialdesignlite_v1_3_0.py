"""
Material Design Lite v1.3.1
"""

from pyno import HTML

HTML.defaults['head'] = {'_before': [
    HTML.Require("https://fonts.googleapis.com/icon?family=Material+Icons", type='css'),
    HTML.Require("https://code.getmdl.io/1.3.0/material.indigo-pink.min.css",
        integrity="sha384-JJ+iHMcntyq9rnkF4oIzyQXskFjLfjcUCV9hE9UjmW1RCJEU58AN2mkle9Bm+YRx",
        crossorigin="anonomous")]}

HTML.defaults['body'] = {'_after': [
    HTML.Require("https://code.getmdl.io/1.3.0/material.min.js", 
        integrity="sha384-KxJ0gpu9LJdUtdV6oe5/l3PSF37awRaWGFhnKt8hq6vS3/Wlzy1YPuJzIwg/ljkW",
        crossorigin="anonomous")]}
