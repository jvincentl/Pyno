# Pyno a nano-framework for generating structured text

Pyno is a light weight collection of functions that allow declarative construction of strucutre text like HTML/SVG/css.
The concept of Pyno is to leverage standard Python features to easily build static pages without too much hassel and without too many restrictions.

Here is a hello world example for an HTML page:

```
from pyno import html as H
H.html(
    H.head(H.style('div {font-weight:bold;font-size:22;')),
    H.body(H.div('Hello there! :)'))
)
```
## Defaults and lazy evaluation

Pyno uses lazy evaluation of elements, which makes it simply to definition of commonly used components, and have them constructed based on whatever defaults are defined for them in the calling site:

```
H.defaults['div'] = {'style': 'background-color:Green;'}
str(H.div('test'))

H.defaults['div'] = {'style': 'background-color:Yellow;'}
str(H.div('test'))

```


## Defining custom components

Custom elements can be defined by defining a class inheriting from the root object H, and defining a construct() method.
The construct method is not called until the component is rendered, and can be accessed through attributes on H, just like typical html tags.

```
from pyno import HTML as H, serve_example
class SpecialButton(H):
    def construct(self, *args, **kwargs):
        return '<button class="SpecialButtonClass" onclick="SpecialButtonJs()">Press me</button>'

serve_example(
    H.html(
        H.body(
            H.div('Hello World'),
            H.SpecialButton())))
```

The previous example shows how you can simply define components using text, however it is also
easy to define custom components using other pyno elements:

```python
class SpecialButton(TreeSub):
    def construct(self):
        return H.button('Press Me', Class='SpecialButtonClass', onclick="SpecialButtonJs()")
```

To define a custom element that takes input, extend the class to store and use the parameters:

```
class SpecialButton(TreeSub):
    def __init__(self, label, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.label = label

    def construct(self, *args, **kwargs):
        return H.button(self.label, Class='SpecialButtonClass', onclick="SpecialButtonJs()")

browser_preview(
    H.html(
        H.body(
            H.div('Hello World'),
            H.SpecialButton('Carry out some kind of action'))))
```

## Defining custom elements using a decorator

Custom elements can also be defined directly from functions by using the decorator H.construct

```python

@H.construct
def SpecialButton(label):
    return H.button(label, Class='SpecialButtonClass', onclick="SpecialButtonJs()")

browser_preview(
    H.html(
        H.body(
            H.div('Hello World'),
            H.SpecialButton('Carry out some kind of action'))))
```