""" This example shows how to define element classes without adding the class attribute and putting the definition up front as is typically the case"""

from pyno import HTML as H

print(H.div.button('sometext'))

# Since the first attribute class definitions are just functors this allows:
button = H.div['btn btn-second']
print(str(button('This is the button text')))

