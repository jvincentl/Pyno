"""

pyno defines a costum element .Require (case sensitive!) that resolves to the appropriate link style depending on the
resource type

"""
from pyno import HTML as H

print(str(H.Require('script.js')))
print(str(H.Require('style.css')))
print(str(H.Require('icon.ico')))
print(H.CDATA('test'))