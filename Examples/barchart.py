from pyno import HTML as H
from pyno import Style

css = Style()
css['svg'] = {'border': '2px solid black'}

H.defaults['svg'] = dict(viewport="0 0 200 200", width=200, height=200)

x, heights = zip(*[(50 + n*(10+1), 80-n*10) for n in range(8)])

page = H.svg(H.style('@import url(/style);'),
             H.circle(cx=55, cy=50, r=10, fill='black'),
             (H.rect(x=x, y=50 - (h) + 80, width=10, height=h) for x, h in zip(x, heights)))

from flask import Flask
app = Flask(__name__)

app.add_url_rule('/', 'home', lambda:page)
app.add_url_rule('/style', 'style', lambda:css)

app.run(debug=True)