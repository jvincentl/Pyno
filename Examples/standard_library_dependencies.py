# When using standard base libraries like bootstrap or Materialize these can be setup simply by importing the corrosponding theme:

# from pyno import HTML as H
# from pyno.themes import bootstrap_v4_4_1

# This sets up the needed _before and _after components in body and head, so as long as the html output contains a full html document
# The proper dependencies will be linked

# The pyno import and theme can also be imported (as HTML with dependencies setup is exposed in the module) eg:

from pyno.themed.bootstrap_v4_4_1 import HTML as H 

print(str(H.html(H.head(), H.body(H.div('test')))))


# The theme simply sets the head and body element _before and _after sections. If they are overwritten the content goes aways:
H.defaults['body'] = {}
H.defaults['head'] = {}
print(str(H.html(H.head(), H.body(H.div('test')))))


# To add additional content, append it

from pyno.themed.materializecss_v1_0_0 import HTML as H 

H.defaults['head']['_before'].append(H.Import('mystyle.css'))

print(str(H.html(H.head(), H.body(H.div('test')))))
