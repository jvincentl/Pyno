""" This example shows how css styles can potentially be handled."""

from pyno import HTML as H, Style
from collections import defaultdict



css = Style()

css.variable.location = 'center'


css['div ul.btn'] = {
    'border_width': '1px',
    'alignment': css.variable.location,
    'background_color': css.variable.primarycolor("#8A64FF")}


css['div ul.btn'] += {'summer': 'Is comming'}
# css['#nav'] = {'alignment': 'top'}


print(str(css))
css.variable.primarycolor = '#E87B59'
print(str(css))

css.variable.location = 'right'
print(str(css))
# Get a list of variables used in the definitions
print(css.list_variables())

"""
# Feature overview
* Variables, both static at top and lazy ones that can be redefined later.
* Nested definition?
* Modular composition across different files
* Import

Mixins

@mixin transform($property) {
  -webkit-transform: $property;
      -ms-transform: $property;
          transform: $property;
}

.box { @include transform(rotate(30deg)); }

* Handle mixins with simple functions
* Extending can be done by simply referencing unwrapped previous styles
* Doing math (You have the entire python language available).

"""