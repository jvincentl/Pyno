from pyno import HTML as H, TreeNode
from werkzeug.serving import run_simple

H.defaults['h1'] = {'style': 'background-color:Blue;'}

if __name__ == '__main__':

    page = H.html(
            H.h1('Hello World'),
            H.div(H.h2('Unordered List of numbers')),
            H.ul(H.li(n) for n in range(1, 10)),
            H.H3('An Example of a scalable vector graphics figure'),
            H.svg(H.circle(cx=50, cy=50, r=20, fill='red'), width=100, height=100),
            H.p('The tree node objects build by pyno support a WSGI Interface'),
            H.p('So they can be passed directly to most python webservers')
        )

    run_simple('localhost', 8080, page, use_reloader=True)