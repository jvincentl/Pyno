"""
This example illustrates how nodes can be attached to flask routes, and also how
arguments can be passed into them using custom flask validators
"""

from pyno import HTML as H
from pyno.wsgi_interface import bond
from pyno import Style

from werkzeug.routing import BaseConverter, ValidationError

css = Style()

css['svg'] = {'border': '2px solid black'}

H.defaults['svg'] = dict(viewport="0 0 200 200", width=200, height=200)

@H.construct
def barchart(heights=None, *args, **kwargs):
    # heights = [int(x.strip()) for x in heights.split(',')]
    auto_x = (50 + n*(10+1) for n in range(len(heights)))
    print(heights)
    return H.svg(H.style('@import url(/style);'),
             H.circle(cx=55, cy=50, r=10, fill='black'),
             (H.rect(x=x, y=50 - (h) + 80, width=10, height=h) for x, h in zip(auto_x, heights)))


class pointset(BaseConverter):
    def to_python(self, value):
        try:
            return [int(x.strip()) for x in value.split(',')]
        except:
            raise ValidationError()

    def to_url(self, values):
        return ','.join(str(x) for x in values)


from flask import Flask
app = Flask(__name__)

app.url_map.converters['pointset'] = pointset

#@app.route('/barchart/<pointset:heights>')
#def barchart(heights): return H.barchart(heights)

# Figure out why app.add_url_rule('/barchart/<pointset:heights>', 'barchart', H.barchart) doesn't work.
# I figured it out. Its because H.barchart is a classmaker object, so getattr() is overwritten. And flask uses getatter(x, 'methods') in order to find if the object defines methods

app.add_url_rule('/style', 'style', lambda:css)
# Note that in order to serve H.constructs it is sometimes needed to terminate them by calling ._view This ends the attribute->class method, which might break the webserver
app.add_url_rule('/barchart/<pointset:heights>', 'barchart', H.barchart._view)


# Directly doesn't work because partial doesn't have a __name__
# app.route('/barchart/<pointset:heights>')(H.barchart)

# app.add_url_rule('/barchart/<pointset:heights>', 'barchart', H.barchart)


print('http://localhost:5000/barchart/10,20,40,30,50')
app.run(debug=True)
