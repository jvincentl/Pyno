"""
This example shows how a user can create personal "tags" which get rendered into the final html during page rendering
which happens when a node object is cast to str or served through a WSGI interface.

"""
from pyno import html as H, serve_example

H.defaults['div'] = {'style': 'background-color:Purple;'}
H.defaults['MyPage'] = {'style': 'background-color:Yellow;'}

class MyPage(H):
    def construct(self, *args, flymetothemoon='sure', **kwargs):
        an_element = H.div('Final remarks')

        value = H.html(
            H.head(H.style('div {font-weight:bold;font-size:22;}')),
            H.body(*self.args, style=kwargs.get('style', '')),
            an_element
        )

        an_element.style = "background-color:green;"

        return value


if __name__ == '__main__':

    serve_example(H.MyPage(H.div('Hello there! :)'), H.div('It`s a meee! Mario!', H.ul(H.li(n) for n in range(1, 10)))))

