from pyno import HTML as H

# Instead of explicitly writing dependencies in the construction. They can be set as _before and _after
# components on the relevant elements.
# This does however require that the user explicity writes html, head and body in the response somewhere, otherwise
# The dependencies will not get parsed in.

H.defaults['head'] = {'_before': [H.Import('style.css')]}
H.defaults['body'] = {'_after': [H.Import('script.js')]}


print(str(H.html(H.head(), H.body(H.div('test')))))