from pyno import html as H, serve_example

H.defaults['div'] = {'style': 'background-color:Green;'}

page = H.html(
        H.head(H.style('div {font-weight:bold;font-size:22;}')),
        H.body(H.div('Hello there! :)'), H.div('It`s a meee!', H.ul(H.li(n) for n in range(1, 10))))
    )

serve_example(page)
