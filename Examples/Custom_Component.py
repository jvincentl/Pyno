from pyno import html as H, serve_example


class SpecialButton(H):
    def __init__(self, label, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.label = label

    def construct(self, *args, **kwargs):
        return H.button(self.label, Class='SpecialButtonClass', onclick="SpecialButtonJs()")

page = H.html(
    H.body(
        H.div('Hello World'),
        H.SpecialButton('hop in da lake')
    )
)

serve_example(page)