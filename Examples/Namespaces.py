""" This example shows how to create a HTML like object with a qname namespace prefix,
and a seperate default value definitions"""
from pyno import HTML as H, Namespace

# To create a new root class simply create a subclass of Namespace and set the namespace variable. Colon is required.
class Y(Namespace):
    _namespace = 'Y:'

H.defaults['div'] = {'defaults_origin': 'html', 'html_flag': True}
Y.defaults['div'] = {'defaults_origin': 'Y', 'y_flag': True}

print(str(H.div()))
print(str(Y.div()))


# Note that assigning a new qname prefix is not necessary to create a seperate default value store.
# And as such, a new object with overlapping namespace can be created simply to manage default values
# Note however that to do so, the subclass must define its own initial default attribute, otherwise the Namespace
# base defaults attribute is used, which is shared amongst all subclasses that do not define their own.

print('\n\nSeperate default Valuse stores:')

class yellowDivs(Namespace):
    defaults = {}
    pass

class blueDivs(Namespace):
    defaults = {}
    pass

yellowDivs.defaults['div'] = {'style': 'background-color:Yellow;'}
blueDivs.defaults['div'] = {'style': 'background-color:Blue;', 'blueflag': 'true'}

print(yellowDivs.div())
print(blueDivs.div())