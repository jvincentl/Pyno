"""

This aims to be a comparison between the speed of setup and render times for:
    * pyno
    * jinja2

And for "low level" bare mimimum compare against:
    * fopen + replace
    * string + replace

"""
from pyno import HTML as H
from dataclasses import dataclass
from jinja2 import Environment, PackageLoader, select_autoescape, FileSystemLoader, BaseLoader
env = Environment(loader=FileSystemLoader('.'))
import time

class ContextTimer:
    def __init__(self, title=''):
        self.start = 0
        self.end = 0
        self.title = title
    def __enter__(self):
        self.start = time.perf_counter_ns()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.end = time.perf_counter_ns()
        print(f'{self.elapsed:8} µs', self.title)

    @property
    def elapsed(self):
        return (self.end - self.start)/10**(9-6)

@dataclass
class User:
    url: str
    username: str
title = 'page title'
users = [User(url='test', username='tom'), User(url='test', username='tim'), User(url='test', username='tad')]
users = users*10
print(users)

main_template = """
<title>{% block title %}{% endblock %}</title>
<ul>
{% for user in users %}
  <li><a href="{{ user.url }}">{{ user.username }}</a></li>
{% endfor %}
</ul>
"""

with ContextTimer('jinja load from file') as T:
    template = env.get_template('main.html')

with ContextTimer('jinja load from string') as T:
    rtemplate = Environment(loader=BaseLoader).from_string(main_template)

with ContextTimer('jinja render') as T:
    container = template.render(title=title, users=users)

with ContextTimer('jinja render from string template') as T:
    container = rtemplate.render(title=title, users=users)

with ContextTimer('pyno template loaded from file') as T:
    from main_template import template

with ContextTimer('pyno function setup') as T:
    def special(title, users):
        return H.title(title, H.ul(H.li(H.a(user.username, href=user.url)) for user in users))

with ContextTimer('pyno render template loaded from file') as T:
    result = str(template(title, users))

with ContextTimer('pyno from function setup render') as T:
    result = str(special(title, users))


with ContextTimer('pyno inline building and render') as T:
    result = str(H.title(title, H.ul(H.li(H.a(user.username, href=user.url)) for user in users)))
