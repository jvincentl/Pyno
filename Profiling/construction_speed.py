""" The purpose of this file is to generate a performance timing of the
html construction to hedge against new features severely degrading performance unknowingly
"""
import git
import time
import shelve
import context

repo = git.Repo(search_parent_directories=True)
sha = repo.head.object.hexsha


from Examples.bootstrap_example import *

start = time.time()
for _ in range(100000):
    constructed_output = H.BootstrapStarterTemplate(H.h1('Hello world!'))
     # t = str(constructed_output) # goes from 3.371 to 5.5589
end = time.time()


with shelve.open('Profiling/performancelog') as db:
    db[sha] = end-start

with shelve.open('Profiling/performancelog') as db:
    for key, value in db.items():
        print(key, value)

print('\ncurrent:', value)


""" 
Going from get_default_args to __kwdefaults__ goes from 3.255507469177246 to 1.6859941482543945
Likely classymaker is taking a lot of that overhead

"""