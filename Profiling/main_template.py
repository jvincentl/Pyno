from pyno import HTML as H


def template(title, users):
    return H.title(title, H.ul(H.li(H.a(user.username, href=user.url)) for user in users))